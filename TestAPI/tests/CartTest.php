
    <?php
    require_once("frontend/controllers/CartUtils.php");

    class CartTest extends \PHPUnit\Framework\TestCase
    {
        public function testAddSessionCart()
        {
            $cart = new CartUtils;
            $oldCount = isset($_SESSION['cart']) ? count(($_SESSION['cart'])) : 0;
            $product = new stdClass();
            $product->name = "John";
            $product->photo = 30;
            $product->giamoi = 12000;
            $result = $cart->addToCart(15, $product);
            $this->assertEquals($oldCount + 1, $result);
        }

        public function testAddSessionCartWithQuantity()
        {
            $cart = new CartUtils;
            $product = new stdClass();
            $product->name = "John";
            $product->photo = 30;
            $product->giamoi = 12000;
            $result = $cart->addToCartWithQuantity(15, $product,10,12);
            $this->assertEquals($_SESSION['cart'][15], $result);
        }

        public function testDeleteCart()
        {
            $cart = new CartUtils;
            $result = $cart->deleteCart(15);
            $this->assertEquals(count($_SESSION['cart']), $result);
        }
    }
