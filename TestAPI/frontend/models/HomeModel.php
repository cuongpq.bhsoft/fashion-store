<?php 
   trait HomeModel{
      public function getData($table, $where, $orderby, $limit, $offset){
         $conn = Connection::getInstance();
         $wh = "";
         $ord = "";
         $lim = "";
         $off = "";
         if($where != null){
            $wh = "where $where";
         }
         if($orderby != null){
            $ord = "order by $orderby";
         }
         if($limit !=null){
            $lim = "limit $limit";
         }
         if($offset != null){
            $off = "offset $offset";
         }
         $sql = "select * from $table $wh $ord $lim  $off";
         $query = $conn->query($sql);

         return $query->fetchAll();
      }

   }
 ?>