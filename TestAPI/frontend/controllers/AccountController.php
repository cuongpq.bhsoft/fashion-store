<?php 
	include "models/AccountModel.php";
	class AccountController extends Controller{
		use AccountModel;
		//dang ky thanh vien
		public function register(){
			$this->loadView("AccountRegisterView.php");
		}		
		
		//khi an nut submit register
		public function registerPost(){
			$fullname = $_POST["fullname"];
			$email = $_POST["email"];
			$address = $_POST["address"];
			$phone = $_POST["phone"];
			$password = $_POST["password"];
			//ma hoa password
			$password = md5($password);
			$userExit = $this->getOneItem(
				"customers",
				"email='$email'"
			);

			if(empty($userExit)){
				//insert ban ghi vao csdl
				$this->saveData(
					"customers",
					"email='$email' ,fullname='$fullname' ,phone='$phone' ,address='$address' ,password='$password'",
					null
				);
				header("location:index.php?controller=account&action=error&message=registerSuccess");
			}else{
				header("location:index.php?controller=account&action=error&message=emailExists");
			}
		}
		//thong bao
		public function notify(){
			$this->loadView("AccountNotifyView.php");
		}

		public function changePassword(){
			$data = [];
			if($_SESSION["customer_email"]){
				$email = $_SESSION["customer_email"];
				$data['user']  = $this->getOneItem(
					"customers",
					"email='$email'"
				);
			}
			
			$this->loadView("ChangePassword.php",$data);
		}
		
		public function error(){
			$this->loadView("AccountErrorView.php");
		}
		//dang nhap
		public function login(){
			$this->loadView("AccountLoginView.php");
		}

		public function doChangePassword(){
			if($_SESSION["customer_email"]){
				$email = $_SESSION["customer_email"];
				$oldPass = $_POST["oldPass"];
				$newPass = $_POST["newPass"];
				$confirmNewPass = $_POST['confirmNewPass'];
				$oldPassCry=  md5($oldPass);
				$user = $this->getOneItem(
					"customers",
					"email='$email'"
				);
				if($user->password != $oldPassCry){
					echo "<script>
						alert('Mật khẩu cũ không đúng, vui lòng thử lại');
						window.location.href='index.php?controller=account&action=changePassword';
					</script>";
				}else{
					$passwordCry = md5($newPass);
					$this->updateData(
						"customers",
						" password='$passwordCry'",
						" email='$email'"
					);
					echo "<script>
						alert('Đổi mật khẩu thành công, Vui lòng đăng nhập lại');
						window.location.href='index.php?controller=account&action=logout';
					</script>";
				}
			}
		}
		//dang nhap an nut submit
		public function loginPost(){
			$email = $_POST["email"];
			$password = $_POST["password"];
			$password = md5($password);
			$response = $_POST['g-recaptcha-response'];

			//---
           $secret = '6Leyaz4aAAAAABNOmkrLybwjIg-yIidhQIv0bx2H'; //Change your google captcha secret here
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response);
            $responseData = json_decode($verifyResponse);
			$user = $this->getOneItem(
				"customers",
				"email='$email'"
			);
			if(!empty($user))
              {if($user->password == $password)
         	   	{$_SESSION["customer_email"] = $email;
					$_SESSION["customer_name"] = $user->fullname;
					$_SESSION["customer_phone"] = $user->phone;
					$_SESSION["customer_id"] = $user->id;
					header("location:index.php");}
              else
              {header("location:index.php?controller=account&action=error&message=passwordExists");}
            }
            else
            {  
            	header("location:index.php?controller=account&action=error&message=accountExists");
            }
                
		}
		//dang xuat
		public function logout(){
			unset($_SESSION["customer_name"]);
			unset($_SESSION["customer_id"]);
			 unset($_SESSION['access_token']);
			header("location:index.php?controller=account&action=login");
		}

			
	}
 ?>