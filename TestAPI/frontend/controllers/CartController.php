<?php
include "models/CartModel.php";
include "CartUtils.php";

class CartController extends Controller
{
	use CartModel;
	//ham tao
	public function __construct()
	{
		//kiem tra neu gio hang chua ton tai thi khoi tao no

		if (!isset($_SESSION["cart"]))
			$_SESSION["cart"] = array();
	}
	//ham hien thi gio hang
	public function index()
	{
		$data['number'] = 0;
		$data['totalPrice'] = 0;
		$data['countCart'] = $this->cartNumber();
		if (isset($_SESSION["cart"])) {
			$data['number'] = count($_SESSION["cart"]);
			$data['totalPrice'] = $this->cartTotal();
		}
		$this->loadView("CartView.php", $data);
	}
	//them san pham vao gio hang
	public function create()
	{	$cartUtils = new CartUtils;
		$id = isset($_GET["id"]) ? $_GET["id"] : 0;
		$size = isset($_GET["size"]) ? $_GET["size"] : 0;
		//goi ham cartAdd tu model de them phan tu vao session array
		$product = $this->getOneItem("product", "id=" . $id);
		$cartUtils->addToCart($id, $product);
		//quay lai trang gio hang
		header("location:index.php?controller=cart");
	}

	// public function addToCart($id, $product)
	// {
	// 	if (isset($_SESSION['cart'][$id])) {
	// 		//nếu đã có sp trong giỏ hàng thì số lượng lên 1
	// 		$_SESSION['cart'][$id]['number']++;
	// 	} else {
	// 		//lấy thông tin sản phẩm từ CSDL và lưu vào giỏ hàng
	// 		//PDO
			
	// 		//---
	// 		$_SESSION['cart'][$id] = array(
	// 			'id' => $id,
	// 			'name' => $product->name,
	// 			'photo' => $product->photo,
	// 			'number' => 1,


	// 			'giamoi' => $product->giamoi
	// 		);
	// 	}
	// 	return count($_SESSION['cart']);
	// }

	//them san pham vao gio hang
	public function createWithNumber()
	{
		$cartUtils = new CartUtils;
		$id = isset($_GET["id"]) ? $_GET["id"] : 0;
		
		$quantity = isset($_GET["quantity"]) ? $_GET["quantity"] : 0;
		$size = isset($_GET["size"]) ? $_GET["size"] : 0;
		$product = $this->getOneItem("products", "id=" . $id);

		$cartUtils->addToCartWithQuantity($id, $product,$quantity, $size);
		//goi ham cartAdd tu model de them phan tu vao session array
		//quay lai trang gio hang
		header("location:index.php?controller=cart");
	}
	public function addToCart()
	{
		$cartUtils = new CartUtils;
		$id = isset($_POST["id"]) ? $_POST["id"] : 0;
		$quantity = isset($_POST["quantity"]) ? $_POST["quantity"] : 0;
		$size = isset($_POST["size"]) ? $_POST["size"] : 0;
		$product = $this->getOneItem("products", "id=" . $id);
		$cartUtils->addToCartWithQuantity($id, $product,$quantity, $size);
		//goi ham cartAdd tu model de them phan tu vao session array
		//quay lai trang gio hang
		$cartCount = $this->cartNumber();
		echo $cartCount;
	}


	//xoa phan tu khoi gio hang
	public function delete()
	{
		$id = isset($_GET["id"]) ? $_GET["id"] : 0;
		$size = isset($_GET["size"]) ? $_GET["size"] : '';
		$cartUtils = new CartUtils;
		$cartUtils->deleteCart($id, $size);
		if(isset($_SESSION['cart']) && count($_SESSION['cart'])> 0){
			header("location:index.php?controller=cart");
		}else{
			header("location:index.php");
		}
	}
	//update so luong san pham trong gio hang
	public function update()
	{
		foreach ($_SESSION["cart"] as $pros => $v) {
			
			foreach ($v as $key => $val) {
				$product_id = $val["id"];
				$size = $val["size"];

				//goi ham update so luong san pham
				$quantity = isset($_POST["product_".$product_id."_".$size]) ? $_POST["product_".$product_id."_".$size] : null;
				if($quantity != null){
					if ($quantity == 0) {
						//xóa sp ra khỏi giỏ hàng
						unset($_SESSION['cart'][$product_id]);
					} else {
						
						$_SESSION['cart'][$product_id][$key]['number'] = $quantity;
					}
				}
			}
			
		}
		//quay lai trang gio hang
		header("location:index.php?controller=cart");
	}
	//thanh toan gio hang
	//thanh toan gio hang
	public function checkOut()
	{
		//kiem tra neu user chua dang nhap thi di chuyen den trang login, nguoc lai thi thanh toan gio hang
		if (isset($_SESSION["customer_id"])) {
			if ($_SESSION["customer_phone"] == 0) {
				header("location:index.php?controller=process&action=personalinfor");
			} else {
				$customer_name = $_SESSION["customer_name"];
				$customer_id = $_SESSION["customer_id"];
				//---
				//---
				//insert ban ghi vao orders, lay order_id vua moi insert
				//lay tong gia cua gio hang
				$result = $this->getLastCustomer($customer_id);
				$_SESSION["customer_phone"] = $result->phone;

				if ($_SESSION["customer_phone"] == 0) {
					header("location:index.php?controller=process&action=personalinfor");
				} else {
					$price = $this->cartTotal();
					$price = $this->insertOrder($customer_id,$customer_name,$price);
				}
				//xoa gio hang
				unset($_SESSION["cart"]);
				//huy gio hang
				$_SESSION["cart"] = array();
				header("location:index.php?controller=account&action=notify&message=checkOutSuccess");
			}
		} else {
			header("location:index.php?controller=account&action=error&message=nologin");
		}
	}
	//xoa gio hang
	public function destroy()
	{
		$this->cartDestroy();
		//quay lai trang gio hang
		header("location:index.php");
	}
}
