<?php
class CartUtils 
{
	//ham tao
	public function __construct()
	{
		//kiem tra neu gio hang chua ton tai thi khoi tao no
		if (!isset($_SESSION["cart"]))
			$_SESSION["cart"] = array();
	}
	//ham hien thi gio hang
	

	public function addToCart($id, $product)
	{
		if (isset($_SESSION['cart'][$id])) {
			//nếu đã có sp trong giỏ hàng thì số lượng lên 1
			$_SESSION['cart'][$id]['number']++;
		} else {
			//lấy thông tin sản phẩm từ CSDL và lưu vào giỏ hàng
			//PDO
			
			//---
			$_SESSION['cart'][$id] = array(
				'id' => $id,
				'name' => $product->name,
				'photo' => $product->photo,
				'number' => 1,


				'giamoi' => $product->giamoi
			);
		}
		return count($_SESSION['cart']);
	}

    public function addToCartWithQuantity($id, $product, $quantity,$size)
	{
	
		if (isset($_SESSION['cart'][$id]) && !empty($_SESSION['cart'][$id]) ) {
			$sizeExist = false;
			foreach ($_SESSION['cart'][$id] as $key => $value) {
				if($value['size'] == $size){
					$sizeExist = true;
					$totalQuan = $value['number'] + $quantity;
				
					$_SESSION['cart'][$id][$key]['number'] = $totalQuan;
		
				}
			}
			if($sizeExist == false){
					$itemCart = array(
						'id' => $id,
						'name' => $product->name,
						'photo' => $product->photo,
						'number' => $quantity,
						'size' => $size,
						'giamoi' => $product->giamoi
					);
					array_push($_SESSION['cart'][$id], $itemCart);
			}
		} else {
			//lấy thông tin sản phẩm từ CSDL và lưu vào giỏ hàng
			//$product = db::get_one("select * from products where id=$id");
			//---
			//PDO
			
			//---
			 $itemCart = array(
				'id' => $id,
				'name' => $product->name,
				'photo' => $product->photo,
				'number' => $quantity,
				'size' => $size,
				'giamoi' => $product->giamoi
			);
			$_SESSION['cart'][$id] = [];
			array_push($_SESSION['cart'][$id], $itemCart);
		}
		
		return $_SESSION['cart'][$id];
	}
    public function deleteCart($id, $size)
	{
		//goi ham cartDelete tu model de xoa phan tu khoi session array
		foreach ($_SESSION["cart"] as $pros => $v) {
			
			foreach ($v as $key => $val) {
				if($val['id'] == $id && $size== $val['size']){
					unset($_SESSION['cart'][$id][$key]);
				}
			}
		}
		//quay lai trang gio hang
		return count($_SESSION['cart']);
	}
}

?>