
<?php 
	//include file model
	include "models/ProductsModel.php";
	class ProductsController extends Controller{
		//su dung file model o day
		use ProductsModel;
		//ham tao
		public function index(){
			//load view
			$this->loadView("LayoutTrangTrong.php");
		}


		public function __construct(){			
		}
		//hien thi danh sach cac ban ghi co phan trang
		public function category(){
			$category_id = isset($_GET["id"]) ? $_GET["id"] : 0;
			$filtersoft = isset($_GET["filtersoft"]) ? $_GET["filtersoft"] : 0;
			$filterprice = isset($_GET["filterprice"]) ? $_GET["filterprice"] : 0;
			
			$p = isset($_GET["p"]) && is_numeric($_GET["p"]) && $_GET["p"] > 0 ? ($_GET["p"]-1) : 0;
			$data['p'] = $p ;
			$data['filtersoft'] = $filtersoft;
			$data['filterprice'] = $filterprice;
			//dinh nghia so ban ghi tren mot trang
			$recordPerPage = 15;
			//tinh tong so trang
			//lay du lieu tu model
			$data['data'] = $this->modelRead($category_id, $recordPerPage, $filtersoft, $filterprice, $p);
			$data['numPage'] = ceil(count($data['data'])/$recordPerPage);
			$data['categories'] = $this->modelListCategories();
			$data['model'] = $this->modelGetCategory($category_id);
			//load view, truyen du lieu ra view
			$this->loadView("ProductsCategoryView.php",$data);
		}
		public function filter(){
			$category_id = isset($_GET["id"]) ? $_GET["id"] : 0;
			//dinh nghia so ban ghi tren mot trang
			$recordPerPage = 15;
			//tinh tong so trang
			$data['numPage'] = ceil($this->totalFilterRecord($category_id,$recordPerPage)/$recordPerPage);
			$data['category_id'] = $category_id;
			//lay du lieu tu model
			$data['data'] = $this->modelFilter($category_id,$recordPerPage);
			$data['categories'] = $this->modelListCategories();
			$data['model'] = $this->modelGetCategory($category_id);
			$data['order'] = $_GET["order"];
			//load view, truyen du lieu ra view
			$this->loadView("ProductsFilterView.php",$data);
		}	
		//chi tiet san pham
		public function detail(){
			$data = [];
			$id= isset($_GET["id"]) ? $_GET["id"] : 0;
			$data['id'] = $id;
			$data['record'] = $this->modelGetRecord($id);
			$data['nosize'] = 0;
			if(empty($data['record']->size1) && empty($data['record']->size2) && empty($data['record']->size3) && empty($data['record']->size4) && empty($data['record']->size5)){
				$data['nosize'] = 1;
			}
			// echo "<pre>";
			// print_r($_SESSION['cart']);die;
			//load view, truyen du lieu ra view
			$this->loadView("ProductsDetailView.php", $data);
		}

		public function detailView(){
			
		    $this->modelHotProducts();
			
			//load view, truyen du lieu ra view
			$this->loadView("ProductsDetailView.php");
		}
		public function ratingProduct(){
			if(isset($_SESSION["customer_id"])){
				$customerId = $_SESSION["customer_id"];
				$productId = isset($_GET["product_id"]) ? $_GET["product_id"] : 0;
				$customerName = $_SESSION["customer_name"];
				$star = isset($_GET["star"]) ? $_GET["star"] : 0;
				$existRating = $this->getRatingByCustomerId($customerId, $productId);
			
				if(isset($existRating) && !empty($existRating)){
					$this->updateRating($existRating->id, $star);
				}else{
					$query = $this->insertRating($customerId,$customerName, $productId, $star);
				}
				echo "<script>
					alert('Đánh giá sản phẩm thành công');
					window.location.href='index.php?controller=products&action=detail&id=".$productId."';
				</script>";
			}else{
				echo "<script>
					alert('Bạn cần đăng nhập trước khi đánh giá');
					window.location.href='index.php?controller=account&action=login';
				</script>";
			}
		}
		public function star(){
			$id = isset($_GET["id"]) ? $_GET["id"] : 0;
			$star = isset($_GET["star"]) ? $_GET["star"] : 0;
			$this->modelComment($id,$star);
		}
	}
 ?>