
<?php 
	//load file model
	include "models/HomeModel.php";
	class HomeController extends Controller{
			use HomeModel;
			
		public function index(){
			$data['hotPromotions'] = $this->getData(
				"promotions",
				null,
				null,
				null,
				null
			);
			$data['hotProducts'] = $this->getData(
				"products",
				"hot=1",
				"id desc",
				5,
				0
			);
			$data['hotProducts2'] = $this->getData(
				"products",
				"hot=1",
				"id desc",
				5,
				5
			);
			$data['categories'] = $this->getData(
				"categories",
				"displayhome=1",
				"id desc",
				null,
				null
			);

			foreach ($data['categories'] as $key => $value) {
				$data['categories'][$key]->products = $this->getData(
					"products",
					"category_id=".$value->id,
					"id desc",
					5,
					0
				);
			}
			// echo "<pre>";
			// print_r($data['hotPromotions']);die;
	
			$this->loadView("HomeView.php",$data);
		}
	}
 ?>