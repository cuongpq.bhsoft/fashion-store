<?php 
     include "models/NewsModel.php";
	class NewsController extends Controller{
		use NewsModel;
		//ham mac dinh
		public function index(){
			$data['hotNews'] = $this->modelHotNews();
			$data['news'] = $this->modelHotNews();
			//load view
			$this->loadView("News.php",$data);
		}

		public function detail(){
			$id = $_GET['id'];
			$data['itemNew'] = $this->getItemNew($id);
			$data['news'] = $this->getListNewOther($id);
			//load view
			$this->loadView("NewDetail.php",$data);
		}
}

   ?>