<?php 
	include "models/CategoriesModel.php";
	class CategoriesController extends Controller{
		use CategoriesModel;
		public function index(){
			$data['categories'] = $this->modelListCategories();
			if(isset($data['categories']) && !empty($data['categories'])){
				foreach ($data['categories'] as $k => $v) {
					$data['categories'][$k]->categoriesSub = $this->modelListCategoriesSub($v->id);
				}
			}
	
			$this->loadView("CategoriesView.php", $data);
		}
	}
 ?>