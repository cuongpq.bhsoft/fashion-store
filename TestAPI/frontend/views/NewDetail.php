<!-- load file layout chung -->
<style>
    .news-content{
        background: #fff;
        border-radius: 10px;
        padding: 40px;
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .news-content h1{
        font-size: 24px;
        margin-top: 0;
    }
    .new-item .new-img {
                display: inline-block;
                height: 175px;
            }
    .new-item{
                height: 276px;
                display: inline-block;
                margin-bottom: 15px;
            }
    .new-item .new-des{
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 4; /* number of lines to show */
                        line-clamp: 4; 
                -webkit-box-orient: vertical;
                height: 69px;
                margin-top: 5px;
            }
</style>
<?php $this->layoutPath = "LayoutTrangTrong.php"; ?>
<div class="news">
    <div class="container">
        <div class="news-content">
            <h1 class="title"><?=$itemNew->name;?></h1>
            <div class="description">
                <?=$itemNew->description;?>
            </div>
            <div class="content">
            <?=$itemNew->content;?>
            </div>
            <div class="orther-news">
                <h2>Tin tức khác: </h2>
                <div class="row">
                     <?php foreach ($news as $rows): ?>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="new-item">
                        <a class="new-img"href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"> <img
                                    src="../assets/upload/news/<?php echo $rows->photo; ?>"
                                    style="width: 100%; "></a><br><b
                                style="font-size: 16px; color: green;"><a
                                    href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"><?php echo $rows->name; ?></a></b>
                            <div class="new-des"><?php echo $rows->description; ?></div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                
               
                    
                
            </div>
        </div>
    </div>
</div>

