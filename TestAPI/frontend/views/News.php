<?php $this->layoutPath = "LayoutTrangTrong.php"; ?>
<div class="new-content">
    <div class="container">
        <div
            style="background-color: white;  border-left: 2px solid purple; border-right: 2px solid black; border-top: 2px solid red; margin-top: 50px;border-radius: 20px 20px 0px 0px;">
            <div
                style="font-size: 25px; font-weight: bold; color: red; margin-left: 25px;font-family: times new roman;text-decoration: underline; padding-top: 20px;">
                TIN TỨC HOT</div>


            <ul class="newshot">
                <?php foreach ($hotNews as $rows): ?>
                <li>
                    <a href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"><img
                            src="../assets/upload/news/<?php echo $rows->photo; ?>"
                            style="width: 150px; vertical-align: top; margin-right: 15px;float: left;"></a><b
                        style="font-size: 16px; color: green;"><a
                            href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"><?php echo $rows->name; ?></a></b>
                    <p><?php echo $rows->description; ?></p>
                </li>
                <?php endforeach; ?>
            </ul>
            <hr style="height: 2px; margin-left: 75px; width: 1060px;background: green;">
            <div
                style="font-size: 20px; font-weight: bold; margin-top: 60px; margin-bottom: 30px;margin-left: 25px;font-family: times new roman;text-decoration: underline;">
                Một số tin tức khác</div>

            <div class="new-list">
                <div class="row">
                    <?php foreach ($news as $rows): ?>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="new-item">
                            <a class="new-img"href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"> <img
                                    src="../assets/upload/news/<?php echo $rows->photo; ?>"
                                    style="width: 100%;  "></a><br><b
                                style="font-size: 16px; color: green;"><a
                                    href="index.php?controller=news&action=detail&id=<?php echo $rows->id; ?>"><?php echo $rows->name; ?></a></b>
                            <div class="new-des"><?php echo $rows->description; ?></div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>


            <style type="text/css">
            .new-list{
                margin-left: 25px;
                margin-right: 25px;
            }
            .new-item{
                height: 276px;
                display: inline-block;
            }
            .new-item .new-des{
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 4; /* number of lines to show */
                        line-clamp: 4; 
                -webkit-box-orient: vertical;
            }
            .new-des{
                height: 69px;
                margin-top: 5px;
            }
            .new-item .new-img {
                display: inline-block;
                height: 175px;
            }
            .newshot {
                list-style: none;
                margin-top: 25px;
                margin-bottom: 50px;
            }

            .newshot>li {
                border: 2px solid #fff;
                width: 850px;
                clear: none;
                padding: 7px;
                font-size: 13px;
                clear: left;
                padding-bottom: 30px;
                margin-bottom: 10px;

            }

            .newshot>li:hover {
                border: 2px solid red;
                border-radius: 0px 20px 20px 0px;
            }


            .news {
                list-style: none;

                margin-top: 25px;
                margin-bottom: 50px;
            }

            .news>li {
                border: 2px solid #fff;
                width: 205px;
                display: inline-block;
                clear: none;
                padding: 7px;
                font-size: 13px;
                clear: left;
                padding-bottom: 30px;
                margin-bottom: 10px;
                margin-right: 10px;

            }

            .news>li:hover {
                border: 2px solid red;
            }
            </style>
        </div>

    </div>
</div>