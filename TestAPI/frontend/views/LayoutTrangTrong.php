<!DOCTYPE html>
<html>

<head>
    <title>FASHION STORE</title>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../assets/frontend/images/logo.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/frontend/css/fl_st_danhmuc.css">
    <style>
	.messenter_popup {
    background: #22b950cc;
    display: inline-block;
    padding: 10px 100px;
    color: #fff;
    font-weight: bold;
    font-size: 18px;
    border-radius: 10px;
    position: fixed;
    z-index: 999999;
    left: -100px;
    top: 50%;
    opacity: 0;
    pointer-events: none;
    transition: .5s;
}
.messenter_popup.messager_active {
    left: 10%;
    top: 20%;
    opacity: 1;
    pointer-events: all;
    transition: .5s;
}
</style>
    <script src="https://code.jquery.com/jquery.js"></script>

</head>

<body>
    <!-- login-->
    <!-- header -->
    <?php include "views/HeaderView.php"; ?>
    <!-- end header -->

    <!-- main -->

    <?php echo $this->view; ?>

    <!-- end main -->
    </div>
    <!-- /sanpham -->
    </div>
    <!-- footer menu -->
    <div class="footer-home">
        <div class="ft-2">&nbsp</div>
        <div class="container">
            <div class="footer-menu">

                <div class="ft-content">
                    <div class="ct1">HỖ TRỢ
                        <ul>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Giúp đỡ</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="ct2">SẢN PHẨM
                        <ul>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Thời trang nam</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Thời trang nữ</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Giày dép nam</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Giày dép nữ</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Túi xách</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Đồng hồ</a></li>
                        </ul>
                    </div>
                    <div class="ct3">CHÍNH SÁCH HỖ TRỢ
                        <ul>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Vận chuyển và trả hàng</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Câu hỏi thường gặp</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Quy chế hoạt động</a></li>
                            <li><i class="fa fa-angle-right"> </i> &nbsp <a href="#">Chính sách bảo mật</a></li>
                        </ul>
                    </div>
                    <div class="ct4">
                        <b><i>Địa chỉ :</i>  <?=$siteInfo->address?></b><br><br>
                        <b style="font-size: 13px;"><i>Tổng đài hỗ trợ :</i>  <?=$siteInfo->phone?><br><br><i>Email :</i>
                        <?=$siteInfo->email?></b>
                    </div>
                </div>

            </div>
        </div>
        <div class="ft-4">
            <div>Copyright © 2020 by NO HOPE (flash-st.com) All rights reserved.</div>
        </div>
    </div>
    <div class="ketqua">
			<h2 class='messenter_popup'>Thêm vào giỏ thành công</h2>
		</div>
    <!-- /footer menu -->
    <button onclick="topFunction()" id="myBtn" title="Go to top">
        <i class="fa fa-arrow-up"></i>
    </button>
    <script type="text/javascript">
    mybutton = document.getElementById("myBtn");
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }


    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    </script>
    <style type="text/css">
    .footer-home {
        background-image: url('../assets/frontend/images/footer-background-img.jpg');
    }

    .footer-menu>.ft-content {
        height: 300px;
    }

    #myBtn {
        cursor: pointer;
        padding: 9px 7px;
        background: red;
        color: yellow;
        position: fixed;
        bottom: 15px;
        left: 20px;
        width: 45px;
        height: 45px;
        z-index: 99;
        animation-name: move;
        animation-duration: 3s;
        animation-iteration-count: infinite;
        border: none;
    }

    @keyframes move {
        0% {
            bottom: 15px;
            left: 20px;
            background: red;
        }

        50% {
            bottom: 80px;
            left: 20px;
            background: purple;
        }

    }

    100% {
        bottom: 15px;
        left: 20px;
        background: red;
    }
    }
    </style>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </script>
    <!-- <div class="zalo-chat-widget" data-oaid="579745863508352884" data-welcome-message="Xin chào !!! Chúng tôi có thể giúp gì cho bạn" data-autopopup="5" data-width="350" data-height="420" data-position="100 100 100 100"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script> -->
    <!-- /quay lai dau trang -->
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v9.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat" attribution=setup_tool page_id="106207104811580"
        logged_in_greeting="Xin chào !!! Chúng tôi có thể giúp gì cho bạn"
        logged_out_greeting="Xin chào !!! Chúng tôi có thể giúp gì cho bạn">
    </div>

</body>

</html>