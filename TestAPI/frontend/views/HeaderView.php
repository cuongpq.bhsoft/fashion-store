<!-- login-->
<style type="text/css">
    .banner>.box{
        margin-left: 150px;
    }
    .banner>.logo{
        margin-left: 26px;
    }
    .banner>.cart{
        margin-left: auto;
    }
    .banner{
        display: flex;
        align-items: center;
    }
    .banner>.logo a img{
        height: 65px;
    }
    .login>.login-icon2 {
    float: right;
    margin-left: auto;
}
.login-header {
    background: #eedbdb;
}
.banner-header{
    background: white;
}
.menu>ul>li .sub-menu {
    display: none;
    position: absolute;
    top: 100%;
    list-style: none;
    z-index: 5;
    left: 0;
    padding: 0;
    width: 190px;
}

.sub-menu>li {
    display: flex;
    border: 1px solid #1f4140;
    border-radius: 10px;
    background-color: #1f4140;
    width: 100%;
    text-align: center;
    
}

.sub-menu>li>a {
    text-decoration: none;
    color: white;
    font-weight: bold;
    width: 100%;
    font-size: 13px;
    padding: 10px 15px;
    display: inline-block;
}

.menu li:hover .sub-menu {
    display: inline-block;
}

.sub-menu li:hover {
    background: black;
}

.sub-menu>li>a:hover {
    color: yellow;
}
.quant-cart{
    font-style: normal;
}
</style>
<?php     include("models/PageModel.php") ;
    $siteInfo = PageModel::getSiteInfomation();

    ?>
<div class="login-header">
    <div class="container">
        <div class="login">
            <div class="login-icon1">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google"></i></a></li>
                </ul>
            </div>
            <div class="login-icon2">
                <ul>
                    <li><i class="fa fa-phone fa-1x"></i>
                        <a href="tel:<?=$siteInfo->phone?>">Hotline: <?=$siteInfo->phone?></a>
                    </li>
                    <li><i class="fa fa-envelope fa-1x"></i>
                        <a href="mailto:<?=$siteInfo->email?>">Email: <?=$siteInfo->email?></a>
                    </li>
                    <!-- <li><i class="fa fa-question-circle fa-1x"></i>
                <a href="#">Trợ giúp</a></li> -->
                    <li><i class="fa fa-sign-in fa-1x"></i>


                        <?php if(isset($_SESSION["customer_name"])): ?>
                        Xin chào <a href="#"
                            style=" font-weight: bold"><?php echo $_SESSION["customer_name"]; ?>(ID:<?php echo $_SESSION["customer_id"]; ?>)</a>
                        | <a href="index.php?controller=account&action=logout">Đăng xuất</a>
            </div>
            <?php else: ?>
            <a href="index.php?controller=account&action=register">Đăng kí</a> |
            <?php unset($_SESSION['access_token']); ?><a href="index.php?controller=account&action=login">Đăng nhập</a>
            </li>
            <?php endif; ?>
            </ul>
        </div>
    </div>

</div>
</div>
<!-- /login -->
<!-- banner -->
<div class="banner-header">
    <div class="container">
        <div class="banner">
            <!-- logo -->
            <div class="logo"><a href="index.php"><img src="../assets/frontend/images/logo.png"></a></div>
            <!-- /logo -->
            <script type="text/javascript">
            function search() {
                var key = document.getElementById('key').value;
                //di chuyen den trang search
                location.href = "index.php?controller=search&action=productName&key=" + key;
            }

            function smartSearch() {
                var key = document.getElementById('key').value;
                if (key != "") {
                    document.getElementById('smart-search').setAttribute("style", "display:block;");
                    //---
                    $.ajax({
                        url: "index.php?controller=search&action=smartSearch&key=" + key,
                        success: function(result) {
                            $("#smart-search ul").empty();
                            $("#smart-search ul").append(result);
                        }
                    });
                } else {
                    document.getElementById('smart-search').setAttribute("style", "display:none;");
                }
            }
            </script>
            <!-- thanh tìm kiếm -->
            <div class="box">
                <div class="search" style="position: relative;">
                    <input type="text" onkeyup="smartSearch();" id="key" placeholder="Nhập tên sản phẩm ..."
                        style="width:100%;height: 35px;border: solid green;background: white;font-size: 13px;float: left;color: black;padding-left: 15px;border-radius: 5px;" />
                </div>
                <div class="icon-search"><button style="height: 35px;    margin-left: -5px;"><i
                            class="fa fa-search fa-1x" onclick="return search();"></i></button></div>
                <style type="text/css">
                #smart-search ul {
                    padding: 0px;
                    margin: 0px;
                    list-style: none;
                }

                #smart-search ul li {
                    border-bottom: 1px solid #dddddd;
                    border-right: 1px solid #dddddd;
                    width: 490px;
                }

                #smart-search {
                    position: absolute;
                    width: 100%;
                    width: 490px;
                    z-index: 1;
                    background: white;
                    display: none;
                    top: 100%;
                }
                </style>
                <div id="smart-search">
                    <ul>
                    </ul>
                </div>
            </div>
            <?php 
        $numberOfProduct = 0;
        if(isset($_SESSION["cart"])){
          foreach($_SESSION["cart"] as $rows)
            foreach($rows as $r)
                $numberOfProduct++;
        }
     ?>

            <div class="cart" id="cart"><a href="index.php?controller=cart"
                    style="font-size: 17px; font-family: times new roman"><i class="fa fa-shopping-cart fa-2x"></i>
                    &nbsp
                    <i class="quant-cart"><?=isset($_SESSION["cart"]) ? $numberOfProduct : "0"; ?></i> sản phẩm</a>


            </div>
        </div>
    </div>
</div>
<!-- /banner -->

<!-- menu -->
<div class="menu-header">
    <div class="container">
        <div class="menu">
            <ul>
                <li><a href="index.php">TRANG CHỦ</a></li>
                <li><a href="index.php?controller=products&action=category&id=33">SẢN PHẨM</a></li>
                <li><a href="index.php?controller=contact&action=checkOut">LIÊN HỆ</a></li>
                <li><a href="index.php?controller=news">TIN TỨC</a></li>
                <li><a href="index.php?controller=cart">GIỎ HÀNG</a></li>
                <li><a>TÀI KHOẢN</a>
                    <ul class="sub-menu">
                        <li><a href="index.php?controller=process&action=checkIn">Thông tin tài khoản</a></li>
                        <li><a href="index.php?controller=process&action=checkOut">Quá trình giao hàng</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div>
