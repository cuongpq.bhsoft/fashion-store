<?php 
include "models/PageModel.php";
	class PageController extends Controller{
        use PageModel;
		public function __construct(){
			$this->authentication();
		}
        public function index(){
			$data = [];
            $data['site'] = $this->getSiteInfomation();
            // echo "<pre>";
            // print_r($data);die;
			$this->loadView("SiteOption.php",$data);
		}
		
        public function update(){
			$data = [];
            $id = $_POST['id'];
            $name = $_POST['name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $address = $_POST['address'];
            $map = $_POST['map'];
            $success = $this->updateSite($id, $name, $email,$phone, $address , $map);
            
			echo "<script>
                alert('Cập nhập thông tin thành công!');
                window.location.href='index.php?controller=page';
            </script>";
		}
	}
 ?>