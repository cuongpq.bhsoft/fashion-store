<!-- load file layout chung -->
<style>
    .page-item{
        margin: 10px 0;
        display: inline-block;
        width: 100%;
    }
    .page-title{
        font-size: 18px;
        color:#000;
        margin: 12px 0;
    }
</style>
<?php 
          $id = $_SESSION["id"];
          $check = $this->modelCheck($id);
         ?>
<?php if ($check == 0): {
    header("location:index.php?controller=users&action=error&message=noRight");
} ?>
<?php else:  ?>
<?php $this->layoutPath = "Layout.php"; ?>
<div class="col-md-12">
    <form action="index.php?controller=page&action=update" method="POST" >
        <div style="margin-bottom:5px;">
            <input type="submit" value="Lưu" class="btn btn-primary" style="border-color: green; background-color: green;">
        </div>
        <div class="panel panel-primary" style="border-color: green;">
            <div class="panel-heading" style="background-color: gray;">Cài đặt trang</div>
            <div class="panel-body">
                <div class="page-item">
                    <h4 class="page-title">Tên công ty</h4>
                    <input type="hidden" name="id" value="<?=$site->id;?>" class="form-control">
                    <div class="page-input">
                        <input type="text" name="name" required value="<?=$site->name;?>" class="form-control" placeholder="Vui lòng nhập tên công ty">
                    </div>
                </div>
                <div class="page-item">
                    <h4 class="page-title">Email</h4>
                    <div class="page-input">
                        <input type="text" name="email" required value="<?=$site->email;?>" class="form-control" placeholder="Vui lòng nhập email">
                    </div>
                </div>
                <div class="page-item">
                    <h4 class="page-title">Phone</h4>
                    <div class="page-input">
                        <input type="text" name="phone" required value="<?=$site->phone;?>" class="form-control" placeholder="Vui lòng nhập Số điện thoại">
                    </div>
                </div>
                <div class="page-item">
                    <h4 class="page-title">Địa chỉ</h4>
                    <div class="page-input">
                        <input type="text" name="address" required value="<?=$site->address;?>" class="form-control" placeholder="Vui lòng nhập địa chỉ">
                    </div>
                </div>
                <div class="page-item">
                    <h4 class="page-title">Google map</h4>
                    <div class="page-input">
                        <input type="text" name="map" value="<?=$site->map;?>" class="form-control" placeholder="Vui lòng nhập id map">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php endif; ?>