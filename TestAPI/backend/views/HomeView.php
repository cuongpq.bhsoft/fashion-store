<!-- load file layout chung -->
<?php $this->layoutPath = "Layout.php"; ?>
<style type="text/css">
	img{margin: auto; margin-top: -27px; margin-left: -10px;}
	.chart-conent{
		display: flex;
	}
    @media only screen and (max-width: 1600px){
        #countries{
            width: 500px !important;
            height: 300px !important;
        }
        #buyers{
            width: 500px !important;
            height: 300px !important;
        }
    }
    @media only screen and (max-width: 1300px){
        #countries{
            width: 400px !important;
            height: 260px !important;
        }
        #buyers{
            width: 400px !important;
            height: 260px !important;
        }
    }
    .chart .chart-num{
        margin-left: 40px;
        margin-bottom: 40px;
    }

</style>
	<div class="chart">
        <div class="chart-num">
            <h2 class="name_chart">Biểu Đồ Thống kê truy cập</h2>
            <div class="number">
                <p style="font-weight: bold;" class="number_get">
                    Hôm nay : <span>12 Lượt truy cập  </span>     Hôm qua:<span>1 Lượt truy cập   </span>     
                </p>
                <p class="number_get">
                    Tuần trước: <span>50 Lượt truy cập </span>      Tổng: <span> 3688 Lượt truy cập </span>  
                </p>
            </div>
        </div>

        <div class="chart-conent">
            <canvas id="countries" width="600" height="400"></canvas>
            <canvas id="buyers" width="600" height="400"></canvas>
        </div>
    </div>
<script>

    var buyerData = {
        labels : ["Thứ 2 ","Thứ 3","Thứ 4","Thứ 5","Thứ 6","Thứ 7"],
        datasets : [
        {
            fillColor : "rgba(172,194,132,0.4)",
            strokeColor : "#ACC26D",
            pointColor : "#fff",
            pointStrokeColor : "#9DB86D",
            data : [203,156,99,251,305,247]
        }
        ]
    }
    
    var buyers = document.getElementById('buyers').getContext('2d');

    new Chart(buyers).Line(buyerData);
    
    var pieData = [
    {
        value: 20,
        color:"#878BB6"
    },
    {
        value : 40,
        color : "#4ACAB4"
    },
    {
        value : 10,
        color : "#FF8153"
    },
    {
        value : 30,
        color : "#FFEA88"
    }
    ];

    var pieOptions = {
        segmentShowStroke : false,
        animateScale : true
    }
    
    var countries= document.getElementById("countries").getContext("2d");
    
    new Chart(countries).Pie(pieData, pieOptions);
    

</script>

