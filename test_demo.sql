-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2022 at 01:08 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `description` varchar(4000) NOT NULL,
  `displayhome` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `photo`, `description`, `displayhome`) VALUES
(28, 0, 'Đồng hồ', '1599760136_bg-dongho.jpg', '<p>Đồng hồ với thiết kế sang trọng v&agrave; thời thượng c&ugrave;ng rất nhiều chất<br />\r\ncũng như m&agrave;u sắc kh&aacute;c nhau sẽ gi&uacute;p n&agrave;ng tr&ocirc;ng nổi bật v&agrave; xinh<br />\r\nđẹp rạng ngời trong mắt người đối diện.</p>\r\n', 1),
(29, 0, 'Túi xách', '1599760172_bg-tuivi.jpg', '<p>Với sự tinh tế trong từng đường n&eacute;t, thanh lịch nhưng cũng đầy c&aacute;<br />\r\nt&iacute;nh v&agrave; vẻ sang trọng trong từng sản phẩm, t&uacute;i x&aacute;ch, mang đến cho<br />\r\nbạn một phong c&aacute;ch đẳng cấp ho&agrave;n to&agrave;n kh&aacute;c biệt.</p>\r\n', 1),
(30, 0, 'Giày dép nữ', '1599760211_giaynu.jpg', '<p>Gi&agrave;y nữ với thiết kế sang trọng v&agrave; thời thượng c&ugrave;ng rất nhiều chất<br />\r\ncũng như m&agrave;u sắc kh&aacute;c nhau sẽ gi&uacute;p n&agrave;ng tr&ocirc;ng nổi bật v&agrave; xinh<br />\r\nđẹp rạng ngời trong mắt người đối diện.</p>\r\n', 1),
(31, 0, 'Giày dép nam', '1599760238_giaynam.jpg', '<p>Gi&agrave;y nam với kiểu d&aacute;ng hiện đại v&agrave; cuốn h&uacute;t - thiết kế sang trọng<br />\r\nc&ugrave;ng chất liệu cao cấp, bền bỉ v&agrave; tỉ mỉ trong từng đường may, chắc<br />\r\nchắn sẽ mang đến vẻ ngo&agrave;i lịch l&atilde;m cho ph&aacute;i mạnh</p>\r\n', 1),
(32, 0, 'Thời trang nữ', '1599760265_thoitrangnu.jpg', '<p>Thời rang nữ với thiết kế sang trọng v&agrave; thời thượng c&ugrave;ng rất nhiều chất<br />\r\ncũng như m&agrave;u sắc kh&aacute;c nhau sẽ gi&uacute;p n&agrave;ng tr&ocirc;ng nổi bật v&agrave; xinh<br />\r\nđẹp rạng ngời trong mắt người đối diện.</p>\r\n', 1),
(33, 0, 'Thời trang nam', '1599760291_thoitrangnam.jpg', '<p>Thời trang nam mang hơi thở, tinh thần của những ch&agrave;ng trai hiện đại.<br />\r\nkh&ocirc;ng c&ocirc; đơn m&agrave; lu&ocirc;n c&oacute; những &acirc;m thanh của tuổi trẻ s&ocirc;i động, nhiệt huyết<br />\r\ntruyền cảm hứng v&agrave; nguồn năng lượng t&iacute;ch cực mỗi ng&agrave;y cho c&aacute;c ch&agrave;ng</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `fullname` varchar(4000) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `picture` varchar(5000) NOT NULL,
  `password` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `email`, `fullname`, `address`, `phone`, `picture`, `password`) VALUES
(20, 'thaolethilqd@gmail.com', 'lê thị thảo', 'Thạch hà-hà tĩnh', '0836388835', '', '17ce28ada967bac52795c98c24ef4c10'),
(50, 'nohope@gmail.com', 'nguyễn hữu trường', 'nhà nghỉ sapa', '0838932315', '', '202cb962ac59075b964b07152d234b70'),
(51, 'truonga2k50ltt@gmail.com1', 'Nguyễn Trường122', 'hà tĩnh', '083893231543', 'https://lh3.googleusercontent.com/a-/AOh14GhkhigvE2iZEknjPhke6cQcLP341LjfTPpJHTfL=s96-c', 'e10adc3949ba59abbe56e057f20f883e'),
(53, 'nohope.nht@gmail.com', 'Nguyễn Trường', 'hà nội', '0839999999', 'https://lh3.googleusercontent.com/-5mbr0aPiNDc/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucn0IxaKIXlleN7TRAYjB17DHC2vug/s96-c/photo.jpg', ''),
(58, 'leduysontung@gmail.com', 'leduysontung', 'th', '0838932315', '', '25f9e794323b453885f5181f1b624d0b'),
(59, 'a@gmail.com', 'nguyễn hữu trường', 'nhà nghỉ sapa', '0838932315', '', '25d55ad283aa400af464c76d713c07ad'),
(60, 'pqcuong1995@gmail.com', 'Quoc Cuong', 'Tieu Son', '0961237324', '', 'e10adc3949ba59abbe56e057f20f883e'),
(61, 'cuongpq.bhsoft@gmail.com', 'Quoc Cuong', 'Tieu Son', '0961237324', '', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `evaluates`
--

CREATE TABLE `evaluates` (
  `id` int(11) NOT NULL,
  `customer_email` varchar(500) NOT NULL,
  `name` varchar(500) NOT NULL,
  `danhgia` varchar(4000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluates`
--

INSERT INTO `evaluates` (`id`, `customer_email`, `name`, `danhgia`) VALUES
(14, 'nva@gmail.com', 'Nguyễn Hữu Trường', '46546hfghfg'),
(19, 'truonga2k50ltt@gmail.com', 'nohope', '123456'),
(23, 'truonga2k50ltt@gmail.com', 'Nguyễn Hữu Trường', '215165132'),
(27, 'thaolethilqd@gmail.com', 'Lê thị thảo', 'Hihi'),
(28, 'dangthuytrang007@gmail.com', 'Vera M', 'Khá ổn'),
(29, 'truonga2k50ltt@gmail.com', 'nohope', 'chất lượng khá tốt'),
(30, 'truonga2k50ltt@gmail.com', 'Nguyễn Hữu Trường', 'shop khá tốt'),
(31, 'truonga2k50ltt@gmail.com', 'Nguyễn Hữu Trường', 'khá tốt'),
(32, 'truong12345k@gmail.com', 'Nguyễn Hữu Trường', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(4000) NOT NULL,
  `content` text NOT NULL,
  `hot` int(11) NOT NULL DEFAULT 0,
  `photo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `content`, `hot`, `photo`) VALUES
(16, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634364_qc2.png'),
(17, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634364_qc2.png'),
(18, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634364_qc2.png'),
(19, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634364_qc2.png'),
(20, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 0, '1601634392_slide2.jpg'),
(21, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634364_qc2.png'),
(22, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634377_qc1.png'),
(23, 'Trump dương tính với nCoV', '<p>Tổng thống Trump th&ocirc;ng b&aacute;o &ocirc;ng c&ugrave;ng vợ đ&atilde; nhận kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV, sau khi một trợ l&yacute; h&agrave;ng đầu của &ocirc;ng nhiễm virus</p>\r\n', '<p>&quot;Đệ nhất phu nh&acirc;n Melania v&agrave; t&ocirc;i đ&atilde; c&oacute; kết quả x&eacute;t nghiệm dương t&iacute;nh với nCoV. Ch&uacute;ng t&ocirc;i sẽ bắt đầu c&aacute;ch ly v&agrave; điều trị ngay lập tức. Ch&uacute;ng t&ocirc;i sẽ vượt qua điều n&agrave;y c&ugrave;ng nhau&quot;, Tổng thống Mỹ Donald Trump h&ocirc;m nay đăng l&ecirc;n Twitter.</p>\r\n\r\n<p>Sau th&ocirc;ng b&aacute;o của Trump, b&aacute;c sĩ Nh&agrave; Trắng cho biết cả hai vợ chồng Tổng thống Mỹ &quot;đều khỏe&quot; v&agrave; họ sẽ c&aacute;ch ly tại Nh&agrave; Trắng. &quot;Họ đều ổn v&agrave;o thời điểm n&agrave;y v&agrave; c&oacute; kế hoạch ở trong Nh&agrave; Trắng trong thời gian dưỡng bệnh&quot;, Sean Conley, b&aacute;c sĩ Nh&agrave; Trắng, cho biết trong tuy&ecirc;n bố. &quot;T&ocirc;i cho rằng Tổng thống sẽ tiếp tục thực hiện nhiệm vụ của m&igrave;nh trong thời gian hồi phục&quot;.</p>\r\n\r\n<p>Nh&agrave; Trắng cũng ra th&ocirc;ng b&aacute;o cho hay họ đ&atilde; bỏ kế hoạch tới bang Florida để vận động tranh cử ra khỏi lịch tr&igrave;nh của Trump. Trump dự kiến tổ chức cuộc mit tinh tại s&acirc;n bay Sanford ở Florida v&agrave;o tối 2/10, nhưng theo lịch tr&igrave;nh mới, &ocirc;ng sẽ chỉ c&oacute; một cuộc trao đổi qua điện thoại về &quot;hỗ trợ người cao tuổi dễ bị tổn thương trước Covid-19&quot;.</p>\r\n\r\n<p>Trước đ&oacute;, Tổng thống Mỹ cho biết vợ chồng &ocirc;ng đ&atilde; bắt đầu qu&aacute; tr&igrave;nh tự c&aacute;ch ly sau khi Hope Hicks, trợ l&yacute; th&acirc;n cận của &ocirc;ng, được x&aacute;c nhận nhiễm virus.</p>\r\n\r\n<p><img alt=\"Trump bên ngoài Nhà Trắng hôm 1/10. Ảnh: AFP.\" src=\"https://i1-vnexpress.vnecdn.net/2020/10/02/trump-3-2298-1601615836.jpg?w=680&amp;h=0&amp;q=100&amp;dpr=1&amp;fit=crop&amp;s=8sTZ92BDv8nD9Yoy2SVl2g\" /></p>\r\n\r\n<p>Trump b&ecirc;n ngo&agrave;i Nh&agrave; Trắng h&ocirc;m 1/10. Ảnh:&nbsp;<em>AFP</em>.</p>\r\n\r\n<p>Một quan chức ch&iacute;nh quyền giấu t&ecirc;n cho hay Hicks, 31 tuổi, được ph&aacute;t hiện dương t&iacute;nh với nCoV h&ocirc;m 1/10, sau khi th&aacute;p t&ugrave;ng Tổng thống đến một cuộc vận động tranh cử ở bang Minnesota. Hicks trước đ&oacute; cũng th&aacute;p t&ugrave;ng Trump trong nhiều chuyến đi gần đ&acirc;y, gồm cuộc tranh luận ở Cleveland h&ocirc;m 29/9.</p>\r\n\r\n<p>H&ocirc;m 30/9, Hicks l&ecirc;n trực thăng Marine One c&ugrave;ng Trump đến cuộc vận động tranh cử ở bang Minnesota. C&aacute;c cố vấn h&agrave;ng đầu kh&aacute;c của Tổng thống như Stephen Miller, Dan Scavino v&agrave; Jared Kushner đi cạnh Hicks nhưng kh&ocirc;ng ai trong số họ đeo khẩu trang.</p>\r\n\r\n<p>Trước Hicks, nhiều nh&acirc;n vi&ecirc;n Nh&agrave; Trắng đ&atilde; nhiễm nCoV, bao gồm Katie Miller, trợ l&yacute; b&aacute;o ch&iacute; của ph&oacute; tổng thống Mike Pence, v&agrave; một số l&iacute;nh cảnh vệ của Trump. Tuy nhi&ecirc;n, Trump đều tuy&ecirc;n bố &ocirc;ng kh&ocirc;ng tiếp x&uacute;c gần với những người n&agrave;y v&agrave; kh&ocirc;ng cần c&aacute;ch ly để ngăn Covid-19.</p>\r\n\r\n<p>Tổng thống Mỹ từng l&agrave;m x&eacute;t nghiệm nCoV hồi th&aacute;ng 3 v&agrave; th&aacute;ng 4, đều nhận kết quả &acirc;m t&iacute;nh. Trump cũng tuy&ecirc;n bố rằng &ocirc;ng v&agrave; c&aacute;c trợ l&yacute; th&acirc;n cận thường xuy&ecirc;n được x&eacute;t nghiệm tại Nh&agrave; Trắng.</p>\r\n\r\n<p>Trump, 74 tuổi, kh&ocirc;ng c&oacute; bệnh l&yacute; nền n&agrave;o, d&ugrave; &ocirc;ng bị b&eacute;o ph&igrave; nhẹ. Kết quả kiểm tra sức khỏe định kỳ lần thứ ba v&agrave;o th&aacute;ng 4 cho thấy &ocirc;ng nặng 110 kg, cao hơn 1,9 m, vượt ngưỡng nhẹ so với c&aacute;c chỉ số bệnh b&eacute;o ph&igrave; theo quy chuẩn của Viện Tim, Phổi v&agrave; M&aacute;u Quốc gia Mỹ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, Tổng thống Mỹ thường xuy&ecirc;n từ chối đeo khẩu trang nơi c&ocirc;ng cộng, d&ugrave; cho rằng khẩu trang c&oacute; t&aacute;c dụng ph&ograve;ng chống Covid-19. Trong cuộc tranh luận tổng thống đầu ti&ecirc;n với ứng vi&ecirc;n đảng D&acirc;n chủ Joe Biden h&ocirc;m 29/9, Trump mang theo khẩu trang nhưng nh&eacute;t trong t&uacute;i &aacute;o, khẳng định &quot;chỉ đeo khi n&agrave;o thấy cần thiết&quot;.</p>\r\n\r\n<p>Trump &iacute;t nhất 15 lần tuy&ecirc;n bố Covid-19 sẽ &quot;<a href=\"https://vnexpress.net/ly-do-trump-tin-ncov-se-bien-mat-ky-dieu-4103246.html\" rel=\"dofollow\">biến mất một c&aacute;ch kỳ diệu</a>&quot;, thậm ch&iacute; từng n&oacute;i đ&ugrave;a về một số phương ph&aacute;p điều trị kh&ocirc;ng c&oacute; cơ sở khoa học như &quot;ti&ecirc;m chất khử tr&ugrave;ng&quot; hoặc &quot;chiếu &aacute;nh s&aacute;ng&quot; v&agrave;o người.</p>\r\n\r\n<p>Th&ocirc;ng tin Trump dương t&iacute;nh với nCoV được c&ocirc;ng bố trong bối cảnh cuộc đua v&agrave;o Nh&agrave; Trắng đ&atilde; tới giai đoạn nước r&uacute;t. &Ocirc;ng c&oacute; kế hoạch tham gia hai cuộc tranh luận trực tiếp cuối c&ugrave;ng với Biden, diễn ra v&agrave;o ng&agrave;y 15/10 v&agrave; ng&agrave;y 22/10.</p>\r\n\r\n<p>Covid-19 đ&atilde; xuất hiện tại hơn 210 quốc gia, v&ugrave;ng l&atilde;nh thổ, khiến hơn 34,4 triệu người nhiễm v&agrave; hơn một triệu người chết. Mỹ đang l&agrave; v&ugrave;ng dịch lớn nhất thế giới với hơn 7,4 triệu ca nhiễm v&agrave; hơn 210.000 ca tử vong.</p>\r\n', 1, '1601634318_thoitrangnam.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(16) NOT NULL,
  `order_id` int(16) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(10) NOT NULL,
  `quantity` int(11) NOT NULL,
  `giamoi` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`id`, `order_id`, `product_id`, `size`, `quantity`, `giamoi`) VALUES
(234, 120, 150, 'XL', 2, 531000),
(235, 121, 5, '36', 1, 1300000),
(236, 122, 134, '0', 1, 1590000),
(237, 123, 142, '0', 2, 439000),
(238, 123, 134, '0', 1, 1590000),
(239, 124, 151, '0', 1, 199000),
(240, 126, 151, 'M', 1, 175000),
(241, 126, 151, 'L', 1, 175000),
(242, 126, 151, 'XXL', 1, 175000),
(243, 126, 149, 'M', 1, 244000),
(244, 127, 151, 'M', 1, 175000),
(245, 128, 134, '0', 2, 1590000),
(246, 128, 151, 'M', 1, 175000),
(247, 128, 151, 'XL', 1, 175000);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `deliveried` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `customer_name`, `date`, `price`, `status`, `deliveried`) VALUES
(120, 58, 'leduysontung', '2021-05-02', 1062000, 3, 1),
(121, 59, 'nguyễn hữu trường', '2021-05-09', 1300000, 3, 1),
(122, 60, 'Quoc Cuong', '2022-05-01', 1590000, 0, 0),
(123, 51, 'Nguyễn Trường', '2022-05-04', 2468000, 0, 0),
(124, 51, 'Nguyễn Trường', '2022-05-04', 199000, 0, 0),
(125, 61, 'Quoc Cuong', '2022-05-10', 2115000, 0, 0),
(126, 61, 'Quoc Cuong', '2022-05-10', 769000, 0, 0),
(127, 61, 'Quoc Cuong', '2022-05-10', 175000, 0, 0),
(128, 61, 'Quoc Cuong', '2022-05-10', 3530000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` varchar(4000) NOT NULL,
  `content` text NOT NULL,
  `hot` int(11) NOT NULL DEFAULT 0,
  `home` int(11) DEFAULT 0,
  `counpon` int(11) DEFAULT 0,
  `photo` varchar(500) NOT NULL,
  `photo1` varchar(500) NOT NULL,
  `photo2` varchar(500) NOT NULL,
  `photo3` varchar(500) NOT NULL,
  `photo4` varchar(500) NOT NULL,
  `photo5` varchar(500) NOT NULL,
  `price` float NOT NULL,
  `giamoi` float NOT NULL,
  `discount` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `size1` varchar(10) NOT NULL,
  `size2` varchar(10) NOT NULL,
  `size3` varchar(10) NOT NULL,
  `size4` varchar(10) NOT NULL,
  `size5` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `content`, `hot`, `home`, `counpon`, `photo`, `photo1`, `photo2`, `photo3`, `photo4`, `photo5`, `price`, `giamoi`, `discount`, `category_id`, `size1`, `size2`, `size3`, `size4`, `size5`) VALUES
(3, 'Giày nam', '', '', 0, 0, 0, '1599554348_giaynam2.jpg', '', '', '', '', '', 1000000, 699000, 31, 31, '', '', '', '', ''),
(5, 'Giày cao gót', '<p>Gi&agrave;y sandal cao g&oacute;t hở mũi với d&acirc;y đeo quấn ngang mắt c&aacute; ch&acirc;n. Bạn kh&ocirc;ng thể bỏ lỡ đ&ocirc;i gi&agrave;y cao g&oacute;t ho&agrave;n hảo n&agrave;y cho những buổi tiệc! Kiểu d&aacute;ng thời trang. Nổi bật l&ecirc;n phong<br />\r\n<br />\r\nc&aacute;ch của bạn. Thiết kế tho&aacute;ng kh&iacute;, hấp thụ sốc khi c&oacute; va chạm mạnh, chống ăn m&ograve;n gi&uacute;p cho đ&ocirc;i gi&agrave;y của bạn lu&ocirc;n như mới.</p>\r\n', '', 1, 0, 0, '1599554297_giaynu5.jpg', '1600183079_giaynu5.jpg', '1600183079_giaynu5-1.jpg', '1600183079_giaynu5-2.jpg', '1600183079_giaynu5-3.jpg', '1600183079_giaynu5-4.jpg', 1699000, 1300000, 35, 30, '35', '36', '37', '38', '39'),
(6, 'Áo phông nam pro', '', '', 1, 0, 0, '1604065612_1599554449_thoitrangnam5.jpg', '1603893460_ap4.jpg', '1603893460_ap1.jpg', '1603893460_ap3.jpg', '1603893460_ap5.jpg', '1603893460_ap2.jpg', 499000, 449000, 12, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(8, 'Túi xách LV', '', '', 1, 0, 0, '1599554568_tuivi1.jpg', '', '', '', '', '', 1200000, 800000, 15, 29, '', '', '', '', ''),
(10, 'Túi  xách nữ Chanel đẹp', '', '', 1, 0, 0, '1599620498_m1-h4.jpg', '', '', '', '', '', 1999000, 1599000, 33, 29, '', '', '', '', ''),
(11, 'Áo sơ mi  trắng nam', '', '', 1, 0, 0, '1599620545_m1-h1.jpg', '1603818410_smt5.jpg', '1603818410_smt3.jpg', '1603818410_smt2.jpg', '1603818410_smt4.jpg', '1603818410_smt1.jpg', 149000, 99000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(12, 'Quần jogger', '', '', 1, 0, 0, '1599760390_thoitrangnam1.jpg', '1603818558_jg5.jpg', '1603818558_jg4.png', '1603818558_jg3.jpg', '1603818558_jg2.jpg', '1603818558_jg1.jpg', 349000, 249000, 30, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(13, 'Đồng hồ nữ', '', '', 1, 0, 0, '1599620776_m1-h5.jpg', '', '', '', '', '', 449000, 299000, 15, 28, '', '', '', '', ''),
(17, 'Quần jogger', '', '', 0, 0, 0, '1599752261_thoitrangnam2.jpg', '1603818506_jg1.jpg', '1603818506_jg2.jpg', '1603818506_jg3.jpg', '1603818506_jg4.png', '1603818506_jg5.jpg', 625000, 549000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(18, 'Quần shock', '', '', 0, 0, 0, '1599760784_thoitrangnam3.jpg', '1603818611_qs1.jpg', '1603818611_qs2.jpg', '1603818611_qs3.jpg', '1603818611_qs4.jpg', '1603818611_qs5.jpg', 199000, 129000, 29, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(19, 'Đầm Họa Tiết Hoa Vàng', '', '', 0, 0, 0, '1599760929_thoitrangnu3.jpg', '', '', '', '', '', 360000, 240000, 33, 32, '', '', '', '', ''),
(21, 'Váy nữ đẹp', '', '', 0, 0, 0, '1599879071_thoitrangnu4.jpg', '', '', '', '', '', 890000, 659000, 20, 32, '', '', '', '', ''),
(22, 'Váy nữ cực xinh', '', '', 0, 0, 0, '1599879123_thoitrangnu1.jpg', '', '', '', '', '', 460000, 379000, 25, 32, '', '', '', '', ''),
(23, 'Giày nam', '', '', 1, 0, 0, '1599879191_giaynam4.jpg', '1603195628_gi1.jpg', '1603195628_gi2.jpg', '1603195628_gi3.jpg', '1603195628_gi4.jpg', '1603195628_gi5.jpg', 790000, 549000, 25, 31, '38', '39', '40', '41', '42'),
(24, 'Giày nam', '', '', 0, 0, 0, '1599879227_giaynam3.jpg', '1603195293_g3.jpg', '1603195293_g4.jpg', '1603195293_g1.jpg', '1603195293_g2.jpg', '1603195293_g5.jpg', 1200000, 1049000, 18, 31, '37', '38', '39', '40', '41'),
(25, 'Giày nam', '', '', 0, 0, 0, '1599879261_giaynam1.jpg', '', '', '', '', '', 780000, 609000, 13, 31, '', '', '', '', ''),
(26, 'Giày  lười nam', '', '', 0, 0, 0, '1599879305_giaynam5.jpg', '', '', '', '', '', 899000, 729000, 15, 31, '', '', '', '', ''),
(27, 'Đồng hồ', '', '', 0, 0, 0, '1599879397_dongho5.jpg', '', '', '', '', '', 1799000, 1599000, 15, 28, '', '', '', '', ''),
(28, 'Đồng hồ nam', '', '', 0, 0, 0, '1599879436_dongho4.jpg', '', '', '', '', '', 1999000, 1799000, 17, 28, '', '', '', '', ''),
(29, 'Đồng hồ nam sành điệu', '', '', 0, 0, 0, '1599879479_dongho3.jpg', '', '', '', '', '', 1499000, 999000, 33, 28, '', '', '', '', ''),
(30, 'Túi xách', '', '', 0, 0, 0, '1599879523_tuivi4.jpg', '', '', '', '', '', 5990000, 5299000, 18, 29, '', '', '', '', ''),
(31, 'Giỏ đi chợ', '', '', 0, 0, 0, '1599879570_tuivi3.jpg', '', '', '', '', '', 499000, 379000, 25, 29, '', '', '', '', ''),
(32, 'Túi xách nữ sành điệu', '', '', 0, 0, 0, '1599879614_tuivi5.jpg', '', '', '', '', '', 290000, 239000, 17, 29, '', '', '', '', ''),
(33, 'Đồng hồ rolex', '', '', 0, 0, 0, '1604065765_1599620430_dongho2.jpg', '', '', '', '', '', 899000, 777000, 18, 28, '', '', '', '', ''),
(34, 'Giày bệt', '', '', 0, 0, 0, '1599879924_giaynu2.jpg', '', '', '', '', '', 699000, 629000, 12, 30, '', '', '', '', ''),
(35, 'Dép bệt', '', '', 0, 0, 0, '1599879964_giaynu1.jpg', '', '', '', '', '', 399000, 279000, 27, 30, '', '', '', '', ''),
(36, 'Dép quai nữ đẹp', '', '', 0, 0, 0, '1599880010_giaynu3.jpg', '', '', '', '', '', 799000, 679000, 18, 30, '', '', '', '', ''),
(37, 'Giày nữ', '', '', 0, 0, 0, '1599880093_giaynu4.jpg', '', '', '', '', '', 1111000, 1009000, 17, 30, '', '', '', '', ''),
(38, 'Đồng hồ', '', '', 0, 0, 0, '1600166013_dongho5.jpg', '', '', '', '', '', 1000000, 790000, 18, 28, '', '', '', '', ''),
(39, 'Đồng hồ1', '', '', 0, 0, 0, '1600246585_dongho4.jpg', '', '', '', '', '', 999000, 762000, 20, 28, '', '', '', '', ''),
(40, 'Đồng hồ nam', '', '', 0, 0, 0, '1604065696_1599879479_dongho3.jpg', '', '', '', '', '', 2800000, 1600000, 33, 28, '', '', '', '', ''),
(41, 'Đồng hồ2', '', '', 0, 0, 0, '1600246667_dongho3.jpg', '', '', '', '', '', 899000, 679000, 22, 28, '', '', '', '', ''),
(43, 'Đồng hồ', '', '', 0, 0, 0, '1600166013_dongho5.jpg', '', '', '', '', '', 1000000, 790000, 18, 28, '', '', '', '', ''),
(44, 'Đồng hồ1', '', '', 0, 0, 0, '1600246585_dongho4.jpg', '', '', '', '', '', 999000, 762000, 20, 28, '', '', '', '', ''),
(46, 'Đồng hồ2', '', '', 0, 0, 0, '1600246667_dongho3.jpg', '', '', '', '', '', 899000, 679000, 22, 28, '', '', '', '', ''),
(47, 'Đồng hồ nữ', '', '', 0, 0, 0, '1600249326_dongho5.jpg', '', '', '', '', '', 789000, 567000, 23, 28, '', '', '', '', ''),
(52, 'Túi xách', '', '', 0, 0, 0, '1599879523_tuivi4.jpg', '', '', '', '', '', 5990000, 5299000, 18, 29, '', '', '', '', ''),
(53, 'Giỏ đi chợ', '', '', 0, 0, 0, '1599879570_tuivi3.jpg', '', '', '', '', '', 499000, 379000, 25, 29, '', '', '', '', ''),
(54, 'Túi xách nữ sành điệu', '', '', 0, 0, 0, '1599879614_tuivi5.jpg', '', '', '', '', '', 290000, 239000, 17, 29, '', '', '', '', ''),
(57, 'Túi xách', '', '', 0, 0, 0, '1599879523_tuivi4.jpg', '', '', '', '', '', 5990000, 5299000, 18, 29, '', '', '', '', ''),
(58, 'Giỏ đi chợ', '', '', 0, 0, 0, '1599879570_tuivi3.jpg', '', '', '', '', '', 499000, 379000, 25, 29, '', '', '', '', ''),
(59, 'Túi xách nữ sành điệu', '', '', 0, 0, 0, '1599879614_tuivi5.jpg', '', '', '', '', '', 290000, 239000, 17, 29, '', '', '', '', ''),
(62, 'Túi xách', '', '', 0, 0, 0, '1599879523_tuivi4.jpg', '', '', '', '', '', 5990000, 5299000, 18, 29, '', '', '', '', ''),
(63, 'Giỏ đi chợ', '', '', 0, 0, 0, '1599879570_tuivi3.jpg', '', '', '', '', '', 499000, 379000, 25, 29, '', '', '', '', ''),
(64, 'Túi xách nữ sành điệu', '', '', 0, 0, 0, '1599879614_tuivi5.jpg', '', '', '', '', '', 290000, 239000, 17, 29, '', '', '', '', ''),
(67, 'Đồng hồ', '', '', 0, 0, 0, '1599879397_dongho5.jpg', '', '', '', '', '', 1799000, 1599000, 15, 28, '', '', '', '', ''),
(68, 'Đồng hồ nam', '', '', 0, 0, 0, '1599879436_dongho4.jpg', '', '', '', '', '', 1999000, 1799000, 17, 28, '', '', '', '', ''),
(69, 'Đồng hồ nam sành điệu', '', '', 0, 0, 0, '1599879479_dongho3.jpg', '', '', '', '', '', 1499000, 999000, 33, 28, '', '', '', '', ''),
(71, 'Giày bệt', '', '', 0, 0, 0, '1599879924_giaynu2.jpg', '', '', '', '', '', 699000, 629000, 12, 30, '', '', '', '', ''),
(72, 'Dép bệt', '', '', 0, 0, 0, '1599879964_giaynu1.jpg', '', '', '', '', '', 399000, 279000, 27, 30, '', '', '', '', ''),
(73, 'Dép quai nữ đẹp', '', '', 0, 0, 0, '1599880010_giaynu3.jpg', '', '', '', '', '', 799000, 679000, 18, 30, '', '', '', '', ''),
(74, 'Giày nữ', '', '', 0, 0, 0, '1599880093_giaynu4.jpg', '', '', '', '', '', 1111000, 1009000, 17, 30, '', '', '', '', ''),
(84, 'Giày bệt', '', '', 0, 0, 0, '1599879924_giaynu2.jpg', '', '', '', '', '', 699000, 629000, 12, 30, '', '', '', '', ''),
(85, 'Dép bệt', '', '', 0, 0, 0, '1599879964_giaynu1.jpg', '', '', '', '', '', 399000, 279000, 27, 30, '', '', '', '', ''),
(86, 'Dép quai nữ đẹp', '', '', 0, 0, 0, '1599880010_giaynu3.jpg', '', '', '', '', '', 799000, 679000, 18, 30, '', '', '', '', ''),
(87, 'Giày nữ', '', '', 0, 0, 0, '1599880093_giaynu4.jpg', '', '', '', '', '', 1111000, 1009000, 17, 30, '', '', '', '', ''),
(89, 'Giày bệt', '', '', 0, 0, 0, '1599879924_giaynu2.jpg', '', '', '', '', '', 699000, 629000, 12, 30, '', '', '', '', ''),
(90, 'Dép bệt', '', '', 0, 0, 0, '1599879964_giaynu1.jpg', '', '', '', '', '', 399000, 279000, 27, 30, '', '', '', '', ''),
(91, 'Dép quai nữ đẹp', '', '', 0, 0, 0, '1599880010_giaynu3.jpg', '', '', '', '', '', 799000, 679000, 18, 30, '', '', '', '', ''),
(92, 'Giày nữ', '', '', 0, 0, 0, '1599880093_giaynu4.jpg', '', '', '', '', '', 1111000, 922000, 17, 30, '', '', '', '', ''),
(113, 'Váy nữ đẹp', '', '', 0, 0, 0, '1603199274_thoitrangnu2.jpg', '', '', '', '', '', 689000, 599000, 15, 32, '', '', '', '', ''),
(114, 'Váy nữ đẹp', '', '', 0, 0, 0, '1599879071_thoitrangnu4.jpg', '', '', '', '', '', 890000, 659000, 20, 32, '', '', '', '', ''),
(115, 'Váy nữ cực xinh', '', '', 0, 0, 0, '1599879123_thoitrangnu1.jpg', '', '', '', '', '', 460000, 379000, 25, 32, '', '', '', '', ''),
(118, 'Đầm Họa Tiết Hoa Vàng', '', '', 0, 0, 0, '1603199254_thoitrangnu3.jpg', '', '', '', '', '', 360000, 240000, 33, 32, '', '', '', '', ''),
(119, 'Váy đẹp', '', '', 1, 0, 0, '1603193729_1599554495_thoitrangnu5.jpg', '', '', '', '', '', 689000, 399000, 45, 32, '', '', '', '', ''),
(134, 'Đồng hồ rolex', '', '', 1, 0, 0, '1603194564_1599620430_dongho2.jpg', '', '', '', '', '', 1590000, 1590000, 0, 28, '', '', '', '', ''),
(137, 'Áo phông nam pro', '', '', 0, 0, 0, '1603893460_ap3.jpg', '', '', '', '', '', 499000, 449000, 12, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(138, 'Áo sơ mi  trắng ', '', '', 0, 0, 0, '1603818410_smt5.jpg', '1599620545_m1-h1.jpg', '1603818410_smt3.jpg', '1603818410_smt2.jpg', '1603818410_smt4.jpg', '1603818410_smt1.jpg', 199000, 109000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(139, 'Quần jogger nam', '', '', 0, 0, 0, '1603818558_jg3.jpg', '1603818558_jg5.jpg', '1603818558_jg4.png', '1599760390_thoitrangnam1.jpg', '1603818558_jg2.jpg', '1603818558_jg1.jpg', 349000, 247000, 30, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(140, 'Quần jogger xịn xò', '', '', 0, 0, 0, '1603818506_jg1.jpg', '1599760390_thoitrangnam2.jpg', '1603818506_jg2.jpg', '1603818506_jg3.jpg', '1603818506_jg4.png', '1603818506_jg5.jpg', 625000, 529000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(141, 'Quần shock', '', '', 0, 0, 0, '1603818611_qs3.jpg', '1603818611_qs1.jpg', '1603818611_qs2.jpg', '1603818611_qs3.jpg', '1603818611_qs4.jpg', '1603818611_qs5.jpg', 199000, 139000, 29, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(142, 'Áo phông nam pro', '', '', 0, 0, 0, '1603893553_ap1.jpg', '1603893553_ap2.jpg', '1603893553_ap5.jpg', '1603893553_ap3.jpg', '1603893553_ap4.jpg', '1603893553_1599554449_thoitrangnam5.jpg', 499000, 439000, 12, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(143, 'Áo sơ mi nam đẹp', '', '', 0, 0, 0, '1603818410_smt3.jpg', '1603818410_smt5.jpg', '1599620545_m1-h1.jpg', '1603818410_smt2.jpg', '1603818410_smt4.jpg', '1603818410_smt1.jpg', 149000, 127000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(144, 'Quần jogger', '', '', 0, 0, 0, '1599760390_thoitrangnam1.jpg', '1603818558_jg5.jpg', '1603818558_jg4.png', '1603818558_jg3.jpg', '1603818558_jg2.jpg', '1603818558_jg1.jpg', 349000, 245000, 30, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(145, 'Quần jogger', '', '', 0, 0, 0, '1599752261_thoitrangnam2.jpg', '1603818506_jg1.jpg', '1603818506_jg2.jpg', '1603818506_jg3.jpg', '1603818506_jg4.png', '1603818506_jg5.jpg', 625000, 539000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(146, 'Quần shock', '', '', 0, 0, 0, '1603818611_qs4.jpg', '1603818611_qs1.jpg', '1603818611_qs2.jpg', '1603818611_qs3.jpg', '1599760784_thoitrangnam3.jpg', '1603818611_qs5.jpg', 199000, 135000, 29, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(147, 'Áo phông nam pro', '', '', 0, 0, 0, '1603893460_ap2.jpg', '1603893460_ap1.jpg', '1599554449_thoitrangnam5.jpg\r\n', '1603893460_ap3.jpg', '1603893460_ap4.jpg', '1603893460_ap5.jpg', 499000, 449000, 12, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(148, 'Áo sơ mi nam', '', '', 0, 0, 0, '1603818410_smt2.jpg', '1603818410_smt5.jpg', '1603818410_smt3.jpg', '1599620545_m1-h1.jpg', '1603818410_smt4.jpg', '1603818410_smt1.jpg', 149000, 127000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(149, 'Quần jogger', '', '', 0, 0, 0, '1603818558_jg1.jpg', '1603818558_jg5.jpg', '1603818558_jg4.png', '1603818558_jg3.jpg', '1603818558_jg2.jpg', '1599760390_thoitrangnam1.jpg', 349000, 244000, 30, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(150, 'Quần jogger nam', '', '', 0, 0, 0, '1599752261_thoitrangnam2.jpg', '1603818506_jg1.jpg', '1603818506_jg2.jpg', '1603818506_jg3.jpg', '1603818506_jg4.png', '1603818506_jg5.jpg', 625000, 531000, 15, 33, 'S', 'M', 'L', 'XL', 'XXL'),
(151, 'Quần short nam', '<p>✔️Giao h&agrave;ng to&agrave;n quốc</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>✔️Cam kết chất lượng</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>✔️Ph&ugrave; hợp với nhiều lứa tuổi</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>✔️Ph&ugrave; hợp với nhiều lứa tuổi</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>✔️Giao h&agrave;ng nhanh</p>\r\n', '<p>⭐⭐⭐ CAM KẾT H&Agrave;NG NHƯ H&Igrave;NH CHỤP - HO&Agrave;N TIỀN NẾU THẤY KH&Ocirc;NG H&Agrave;I L&Ograve;NG.</p>\r\n\r\n<p>⭐⭐⭐ H&Atilde;Y INBOX CHO SHOP KHI SẢN PHẨM C&Oacute; VẤN ĐỀ ( ĐỔI SIZE, SP BỊ LỖI...) ĐỂ HỖ TRỢ TRƯỚC KHI Đ&Aacute;NH GI&Aacute;.</p>\r\n\r\n<p>✔️ QUẦN JEANS BAGGY D&Aacute;NG ỐNG SU&Ocirc;NG NAM CAO CẤP</p>\r\n\r\n<p>- C&oacute; phải bạn đang muốn t&igrave;m cho m&igrave;nh một chiếc quần jean baggy cao cấp mang style h&agrave;n quốc?</p>\r\n\r\n<p>May mắn l&agrave; bạn đ&atilde; t&igrave;m thấy ch&uacute;ng t&ocirc;i.</p>\r\n\r\n<p>- Chiếc quần jean baggy d&agrave;nh cho nam n&agrave;y cửa h&agrave;ng ch&uacute;ng t&ocirc;i b&aacute;n khoảng 600 chiếc mỗi th&aacute;ng. Đ&atilde; b&aacute;n hơn 6.000 chiếc chỉ t&iacute;nh ri&ecirc;ng tr&ecirc;n hệ thống của Shopee Việt Nam, chưa kể đến những k&ecirc;nh b&aacute;n kh&aacute;c! - Bạn cũng sẽ l&agrave; một trong số những người sẽ sở hữu chiếc quần jean baggy mang phong c&aacute;ch h&agrave;n quốc n&agrave;y.</p>\r\n\r\n<p>Bởi v&igrave; đ&acirc;y l&agrave; một chiếc quần jean m&agrave; cực kỳ dễ phối đồ từ &aacute;o thun, hoodie, &aacute;o kho&aacute;c..cho đến c&aacute;c loại sneakers, boots..</p>\r\n\r\n<p>✔️ Th&ocirc;ng Tin Sản Phẩm: - Kiểu D&aacute;ng: quần jean baggy d&agrave;nh cho nam - Mầu Sắc: Xanh Sky, Đen full, Xanh nhạt - Chất liệu: jean cao cấp, ko phai mầu - Số lượng : h&agrave;ng đủ size , xuất khẩu - gồm c&oacute; đủ size: từ size 26 ( 42kg) -&gt; size 37 (110kg). (FILE ẢNH GỐC TR&Ecirc;N)</p>\r\n', 1, 1, 0, '1603818611_qs5.jpg', '1603818611_qs1.jpg', '1603818611_qs2.jpg', '1603818611_qs3.jpg', '1603818611_qs4.jpg', '1599760784_thoitrangnam3.jpg', 199000, 175000, 12, 33, 'S', 'M', 'L', 'XL', 'XXL');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `photo1` varchar(500) NOT NULL,
  `photo2` varchar(500) NOT NULL,
  `photo3` varchar(500) NOT NULL,
  `photo4` varchar(500) NOT NULL,
  `photo5` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `photo`, `photo1`, `photo2`, `photo3`, `photo4`, `photo5`) VALUES
(2, '1605067336_slide1.jpg', '1605067336_slide2.jpg', '1605067336_slide3.jpg', '1605067336_slide4.jpg', '1605067336_slide5.jpg', '1605067336_slide6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `product_id` int(11) NOT NULL,
  `star` int(11) NOT NULL,
  `danhgia` varchar(4000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `customer_id`, `customer_name`, `product_id`, `star`, `danhgia`) VALUES
(55, 51, 'Nguyễn Trường', 5, 5, '2'),
(57, 51, 'Nguyễn Trường', 12, 5, 'hello'),
(58, 51, 'Nguyễn Trường', 11, 4, '33'),
(61, 50, '', 151, 5, ''),
(62, 58, 'leduysontung', 150, 5, 'aaaaaaa'),
(64, 51, 'Nguyễn Trường1', 151, 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_option`
--

CREATE TABLE `site_option` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `map` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_option`
--

INSERT INTO `site_option` (`id`, `name`, `email`, `phone`, `address`, `map`) VALUES
(1, 'cong ty testzxcz22', 'test@gmail.com', '0961237324', 'Số 147 phố Mai Dịch, Mai Dịch, Cầu Giấy Hà Nội.', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `manager` int(11) NOT NULL,
  `admin_products` int(11) NOT NULL,
  `admin_news` int(11) NOT NULL,
  `admin_orders` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `manager`, `admin_products`, `admin_news`, `admin_orders`) VALUES
(21, 'Nguyễn Hữu Trường', 'nohope@gmail.com', 'a370c69fa30877904a5af413cdc299f1', 0, 1, 0, 0),
(23, 'Nguyễn Hữu Trường', 'truonga2k50ltt@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, 1),
(30, 'Quoc Cuong', 'cuongpq.bhsoft@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluates`
--
ALTER TABLE `evaluates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `products` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_option`
--
ALTER TABLE `site_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `evaluates`
--
ALTER TABLE `evaluates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `site_option`
--
ALTER TABLE `site_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
